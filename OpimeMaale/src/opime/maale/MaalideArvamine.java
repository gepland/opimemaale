package opime.maale;

import java.awt.EventQueue;
import java.awt.Image;
import java.io.File;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JPanel;
import java.awt.Color;

public class MaalideArvamine {
	
	File file = new File("src/images");
	String[] imageNames = file.list(); 
	Random rand = new Random();
	int pildiNumber, kokku, oigeid, uusW, uusH;
	double w, h;

	private JFrame frame;
	private JTextField kiriVastus;

	/**
	 * Rakenduse k�ivitamine.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MaalideArvamine window = new MaalideArvamine();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Rakenduse loomine.
	 */
	public MaalideArvamine() {
		initialize();
	}

	/**
	 * Raami sisu esitamine.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel pilt = new JLabel("<html>Alustamiseks vajuta nuppu: Alusta!"
				+ "<br><br>Peale oma vastuse sisestamist vajuta nuppu: Vasta"
				+ "<br><br>Kui oled �igest arvanud l�heb keskmine kast roheliseks ja vale vastuse korral punaseks."
				+ "<br><br>J�rgmise suvalise pildi saamiseks kl�psa nuppu: J�rgmine Pilt</html>");
		pilt.setBounds(37, 11, 360, 360);
		frame.getContentPane().add(pilt);
		
		JPanel marker = new JPanel();
		marker.setBackground(Color.WHITE);
		marker.setBounds(168, 417, 98, 59);
		frame.getContentPane().add(marker);
		
		JLabel skoor = new JLabel("Oled vastanud �igesti " + 0 + " korda " + 0 + " korrast.");
		skoor.setBounds(10, 487, 414, 14);
		frame.getContentPane().add(skoor);
		
		kiriVastus = new JTextField();
		kiriVastus.setBounds(10, 386, 414, 20);
		frame.getContentPane().add(kiriVastus);
		kiriVastus.setColumns(10);
		
		JButton btnVasta = new JButton("Vasta");
		btnVasta.setEnabled(false);
		btnVasta.setBounds(276, 417, 148, 59);
		frame.getContentPane().add(btnVasta);
		
		JButton btnAlgus = new JButton("Alusta!");
		btnAlgus.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent evt) 
			{
				btnAlgus.setText("J�rgmine pilt");
				btnVasta.setVisible(false);
				pilt.setText("");
				marker.setBackground(Color.WHITE);
				
			    pildiNumber = rand.nextInt(imageNames.length);
			    
			    /**
			     * Pildi kaustast leidmine ja m��tude v�tmine.
			     */

			    Image pic = new ImageIcon(this.getClass().getResource("/images/" + imageNames[pildiNumber])).getImage();		    			    
			    	w = pic.getWidth(pilt);
			    	h = pic.getHeight(pilt);			    
			    	uusH = (int) (360 / (w / h));			    
			    	uusW = (int) (360 / (h / w));
			    	
			    /**
			     * Vastavalt JLabeli m��tudele pildi m��tude korrigeerimine.
			     */
			    
			    if (w > h){
			    	Image pict = new ImageIcon(this.getClass().getResource("/images/" + imageNames[pildiNumber])).getImage().getScaledInstance(360, uusH, Image.SCALE_DEFAULT);
			    	ImageIcon iconLogo = new ImageIcon(pict);
			    	pilt.setIcon(iconLogo);
			    } else {
			    	Image pict = new ImageIcon(this.getClass().getResource("/images/" + imageNames[pildiNumber])).getImage().getScaledInstance(uusW, 360, Image.SCALE_DEFAULT);
			    	ImageIcon iconLogo = new ImageIcon(pict);
			    	pilt.setIcon(iconLogo);
			    }			    
			    
			    	JButton button = new JButton("Vasta");
			    	button.setEnabled(true);
			    	button.addActionListener(new ActionListener() {
			    		@Override
			    		public void actionPerformed(ActionEvent e) {		    		
			    		
			    		
			    			/**
			    			 * V�tab faili aadressist v�lja pildi nime.
			    			 * V�rdleb seda kasutaja poolt antud vastusega.
			    			 */
			    			String aadress = "/images/" + imageNames[pildiNumber];
			    			int nimeAlgus = aadress.lastIndexOf('/');
			    			int nimeLopp = aadress.lastIndexOf('.');
			    		
			    			String nimi = aadress.substring(nimeAlgus+1, nimeLopp);
			    			
			    			kokku = kokku + 1;
			    		
			    			String vastus = kiriVastus.getText();
			    			if (vastus.equals(nimi)) {
			    				marker.setBackground(Color.GREEN);
			    				kiriVastus.setText(null);
			    				oigeid = oigeid + 1;
			    			} else {
			    				marker.setBackground(Color.RED);
			    			}
			    			
			    			skoor.setText("Oled vastanud �igesti " + oigeid + " korda " + kokku + " korrast.");
			    			
			    		}
			    	});
			    	button.setBounds(276, 417, 148, 59);
			    	frame.getContentPane().add(button);
			    	
			}
		});
		btnAlgus.setBounds(10, 417, 148, 59);
		frame.getContentPane().add(btnAlgus);
		
	}
}
